class Compras {
    constructor (name, id, preco, links, reserva_name, reserva_isReserved, reserva_maxReserv, img){
        this.name = name;
        this.id = id;
        this.preco = preco;
        this.links = links;
        this.reserva = {};
        this.reserva.name = reserva_name;
        this.reserva._isReserved = reserva_isReserved;
        this.reserva._max = reserva_maxReserv;
        this.img = img;
    }

    get isReserved(){
        return (this.reserva._isReserved !== this.reserva._max) ? "chose" : "chosen";
    }

    set isReserved(value){
        if(typeof value === 'boolean'){
            this.reserva._isReserved = value;
        }
    }

    get max(){
        return this.reserva._max;
    }
}

function ProductFiller(objectArray){
    return new Compras(objectArray.name, objectArray.id, objectArray.preco, objectArray.links, objectArray.reserva.name, objectArray.reserva.isReserved, objectArray.reserva.maxReserv, objectArray.img);
}

const getData = () => {
    let data = `[
        {
            "name":"Liquidificador com filtro (220v)",
            "id": 0,
            "preco": "100",
            "links":["https://tinyurl.com/y7j2zhyx"],
            "reserva":{
                "name":"Liquidificador",
                "isReserved":0,
                "maxReserv":1
            },
            "img": "./img/liquidificador.jpg"
        },
        {
            "name":"Cadeira de praia",
            "id": 1,
            "preco": "32.99",
            "links":["https://tinyurl.com/y9fh8uxh"],
            "reserva":{
                "name":"Cadeira De Praia",
                "id":"cadeiraPraia",
                "isReserved":0,
                "maxReserv":1
            },
            "img": "./img/cadeira.jpg"
        },
        {
            "name":"Batom 24h",
            "id": 2,
            "preco": "80.00",
            "links":["https://tinyurl.com/y7achzma"],
            "reserva":{
                "name":"Batom 24h",
                "isReserved":0,
                "maxReserv":1
            },
            "img": "./img/batom.jpg"
        },
        {
            "name":"Spot Ligth (x5)",
            "id": 3,
            "preco": "50 150",
            "links":[
                "https://tinyurl.com/ycvnr5m8",
                "https://tinyurl.com/y8gu4myf"
            ],
            "reserva":{
                "name":"Spot Ligths",
                "isReserved":0,
                "maxReserv":1
            },
            "img": "./img/spot.jpg"
        },
        {
            "name":"Jarra de Vidro Grosso",
            "id": 4,
            "preco": "20 100",
            "links":[
                "https://tinyurl.com/yaebvygd"
            ],
            "reserva":{
                "name":"Jarra de Vidro Grosso",
                "isReserved":0,
                "maxReserv":2
            },
            "img": "./img/jarra.jpg"
        },
        {
            "name":"Blackout",
            "id": 5,
            "preco": "100",
            "links":[
                
            ],
            "reserva":{
                "name":"Blackout",
                "isReserved":0,
                "maxReserv":1
            },
            "img": "./img/cortina.jpg"
        },
        {
            "name":"Utensílios de Cozinha para teflon",
            "id": 6,
            "preco": "--",
            "links":[
                
            ],
            "reserva":{
                "name":"Utensílios de Cozinha",
                "isReserved":0,
                "maxReserv":1
            },
            "img": "./img/utensilios.jpg"
        },
        {
            "name":"Geladeira TOP",
            "id": 7,
            "preco": "1500 4000",
            "links":[
                "https://tinyurl.com/yx8kp4nw",
                "https://tinyurl.com/y4eajphn"
            ],
            "reserva":{
                "name":"Geladeira",
                "isReserved": 0,
                "maxReserv":1
            },
            "img": "./img/geladeira.jpg"
        },
        {
            "name":"Tábua de Carne",
            "id": 8,
            "preco": "37",
            "links":[
                "https://tinyurl.com/y4fdzvn8"
            ],
            "reserva":{
                "name":"Tábua de carne",
                "isReserved":0,
                "maxReserv":1
            },
            "img": "./img/tabua.jpg"
        }
    ]`;

    return data;
}

/*
{
    "name":"",
    "id": ,
    "preco": "",
    "links":[
        
    ],
    "reserva":{
        "name":"",
        "isReserved":0,
        "maxReserv":1
    },
    "img": "./img/"
}
*/